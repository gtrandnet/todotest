import Vue from 'vue'
import Router from 'vue-router'
import store from './store/store'

Vue.use(Router)
function LogOut(from, to, next) {
  store.commit('auth/UNSET_TOKEN');
  next('/login');
}



const router = new Router({
    mode: 'history',
    routes: [

        {
            path: '',
            component:() => import('./components/Main.vue'),
            children: [
              {
                path: '/',
                name: 'home',
                components:{
                    main:() => import('./views/Home.vue')
                }
              },
              {
                path: '/profile/:userId',
                name: 'profile',
                components:{
                    main:() => import('./views/profile/Profile.vue')
                }
              },
              {
                path: '/login',
                name: 'login',
                components: {
                    main:() => import('./views/auth/Login.vue')
                }
              },
            ]
        },
        // Redirect to 404 page, if no match found
        {
          name: 'logout',
          path: '/logout',
          beforeEnter: LogOut
        },
        {
            path: '*',
            redirect: '/pages/error-404'
        }
    ],
})

router.beforeEach((to, from, next) => {
  if (to.name !== 'login' && !store.getters['auth/getToken']) next({ name: 'login' })
  else next()
})

export default router
