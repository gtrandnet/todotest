import Vue from 'vue'
import App from './App.vue'
import VModal from 'vue-js-modal'
import Notifications from 'vue-notification'

Vue.config.productionTip = false
Vue.use(VModal, { dynamic: true, dynamicDefaults: { clickToClose: false } })
Vue.use(Notifications)

// axios
import axios from "./axios.js"
Vue.prototype.$http = axios

Vue.prototype.$http.interceptors.request.use(config => {
  config.headers.Authorization = 'Bearer ' + localStorage.getItem("todo-user-token");
  return config
});

Vue.prototype.$apiUrl = process.env.VUE_APP_API_URL

// Vue Router
import router from './router'

// Vuex Store
import store from './store/store'

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
