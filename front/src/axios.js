// axios
import axios from 'axios'

const baseURL = process.env.VUE_APP_API_URL


export default axios.create({
  baseURL: baseURL,
  headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
  }
});
