export default {
    SET_USERS(state, users) {
        state.users = users
    },

    SET_USER(state, user) {
        state.users.push(user)
    },

    DELETE_USER(state, user) {
        const index = state.users.findIndex(el => el.id === user.id)
        state.users.splice(index, 1)
    },

    UPDATE_USER(state, user){
        const index = state.users.findIndex(el => el.id === user.id)
        state.users.splice(index, 1, user)
    },
}
