export default {
    GET_USERS({commit}, payload){
        payload.$http.get(`user?page=${payload.page}`).then(response => {
            commit('SET_USERS', response.data.users.data)
            payload.pagination = response.data.users
        })
    },

    CREATE_USER({commit}, payload){
        payload.$http.post('user', payload.user).then(response => {
            payload.clearModal()
            commit('SET_USER', response.data.user)
            payload.$notify({
                group: 'notify',
                title: 'Ураа...',
                text: 'Процесс успешно прошел!',
                position: ['bottom', 'right'],
                type: 'success',
                duration: 3500,
            })
        }).catch(() => {
            payload.$notify({
                group: 'notify',
                title: 'Ошибка',
                text: 'Возникла непредвиденная ошибка!',
                position: 'bottom right',
                type: 'warning',
                duration: 3500,
            })
        })
    },

    UPDATE_USER({commit}, payload){
        payload.$http.put(`user/${payload.userId}`, payload.user).then(response => {
            commit('UPDATE_USER', response.data.user)
            payload.$notify({
                group: 'notify',
                title: 'Ураа...',
                text: 'Процесс успешно прошел!',
                position: ['bottom', 'right'],
                type: 'success',
                duration: 3500,
            })
        }).catch(() => {
            payload.$notify({
                group: 'notify',
                title: 'Ошибка',
                text: 'Возникла непредвиденная ошибка!',
                position: 'bottom right',
                type: 'warning',
                duration: 3500,
            })
        })
    },

    DELETE_USER({commit}, payload){
        payload.$http.delete(`user/${payload.deleteUser.id}`).then(() => {
            commit('DELETE_USER', payload.deleteUser)
            payload.$notify({
                group: 'notify',
                title: 'Ураа...',
                text: 'Процесс успешно прошел!',
                position: ['bottom', 'right'],
                type: 'success',
                duration: 3500,
            })
        }).catch(() => {
            payload.$notify({
                group: 'notify',
                title: 'Ошибка',
                text: 'Возникла непредвиденная ошибка!',
                position: 'bottom right',
                type: 'warning',
                duration: 3500,
            })
        })
    }
}
