export default {
    SET_USER_TODOS(state, todos) {
        state.userTodos = todos
    },

    SET_USER_TODO(state, todo) {
        state.userTodos.push(todo)
    },

    DELETE_USER_TODO(state, todo) {
        const index = state.userTodos.findIndex(el => el.id === todo.id)
        state.userTodos.splice(index, 1)
    },

    UPDATE_USER_TODO(state, todo){
        const index = state.userTodos.findIndex(el => el.id === todo.id)
        state.userTodos.splice(index, 1, todo)
    },
}
