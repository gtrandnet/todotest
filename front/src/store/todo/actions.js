export default {
    GET_USER_TODOS({commit}, payload){
        payload.$http.get(`user/todo/${payload.userId}`).then(response => {
            commit('SET_USER_TODOS', response.data.user.todos)
            payload.user.name = response.data.user.name
            payload.user.email = response.data.user.email
            payload.user.password = response.data.user.password
        })
    },

    CREATE_USER_TODO({commit}, payload){
        const data = {
            user_id: payload.userId,
            title: payload.title
        }
        payload.$http.post('user/todo', data).then(response => {
            commit('SET_USER_TODO', response.data.todo)
            payload.title = null
            payload.$notify({
                group: 'notify',
                title: 'Ураа...',
                text: 'Процесс успешно прошел!',
                position: ['bottom', 'right'],
                type: 'success',
                duration: 3500,
            })
        }).catch(() => {
            payload.$notify({
                group: 'notify',
                title: 'Ошибка',
                text: 'Возникла непредвиденная ошибка!',
                position: 'bottom right',
                type: 'warning',
                duration: 3500,
            })
        })
    },

    UPDATE_USER_TODO({commit}, payload){
        payload.$http.put(`user/todo/${payload.editTodoId}`, {title:payload.title}).then(response => {
            commit('UPDATE_USER_TODO', response.data.todo)
            payload.title = null
            payload.editTodoId = null
            payload.$notify({
                group: 'notify',
                title: 'Ураа...',
                text: 'Процесс успешно прошел!',
                position: ['bottom', 'right'],
                type: 'success',
                duration: 3500,
            })
        }).catch(() => {
            payload.$notify({
                group: 'notify',
                title: 'Ошибка',
                text: 'Возникла непредвиденная ошибка!',
                position: 'bottom right',
                type: 'warning',
                duration: 3500,
            })
        })
    },

    DELETE_USER_TODO({commit}, payload){
        payload.$http.delete(`user/todo/${payload.deleteTodo.id}`).then(() => {
            commit('DELETE_USER_TODO', payload.deleteTodo)
            payload.$notify({
                group: 'notify',
                title: 'Ураа...',
                text: 'Процесс успешно прошел!',
                position: ['bottom', 'right'],
                type: 'success',
                duration: 3500,
            })
        }).catch(() => {
            payload.$notify({
                group: 'notify',
                title: 'Ошибка',
                text: 'Возникла непредвиденная ошибка!',
                position: 'bottom right',
                type: 'warning',
                duration: 3500,
            })
        })
    }
}
