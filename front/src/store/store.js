import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth/'
import user from './user/'
import todo from './todo/'

Vue.use(Vuex)


export default new Vuex.Store({
    modules: {
        auth: auth,
        user: user,
        todo:todo
    },
    // strict: process.env.NODE_ENV !== 'production'
})
