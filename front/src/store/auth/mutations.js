export default {
    SET_TOKEN(state, response) {
        localStorage.setItem('todo-user-token', response[0]);
        localStorage.setItem('todo-user-profile', JSON.stringify(response[1]));
        state.token = response[0];
        state.user = JSON.stringify(response[1]);
        state.isAuthenticated = true
    },

    UNSET_TOKEN(state) {
        state.isAuthenticated = false;
        state.token = null;
        state.user = null;
        localStorage.removeItem('todo-user-token');
        localStorage.removeItem('todo-user-profile');
    },

    UPDATE_USER_DATA(state, user_data){
        localStorage.setItem('todo-user-profile', user_data);
        state.user = user_data;
    },
}
