export default {
    isAuthenticated: false,
    error: false,
    token: localStorage.getItem('todo-user-token') || null,
    user: localStorage.getItem('todo-user-profile') || null,
}
