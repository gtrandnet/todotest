export default {
    isUserAuthenticated: state => {
        if (state.token) {
            state.isAuthenticated = true
        }
        return state.isAuthenticated
    },

    getToken: state => {
        return state.token
    },

    getProfile: state => {
        return state.user
    },
}
