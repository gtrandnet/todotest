import router from '../../router'

export default {
    LOGIN({commit}, payload) {
        const data = {
            email: payload.email,
            password: payload.password
        }
        payload.$http.post('login', data).then(response => {
            commit('SET_TOKEN', [response.data.meta.token, response.data.data]);

            router.go(-1);

        }).catch(() => {
            payload.$notify({
                group: 'notify',
                title: 'Ошибка',
                text: 'Неверные данные!',
                position: 'bottom right',
                type: 'warning',
                duration: 3500,
            })
        })
    },

    REGISTER({commit}, payload){
        const data = {
            name:payload.name,
            password:payload.password,
            email:payload.email
        }

        payload.$http.post('register', data).then(response => {
            commit('SET_TOKEN', [response.data.meta.token, response.data.data]);
        }).catch(() => {
            payload.$notify({
                group: 'notify',
                title: 'Ошибка',
                text: 'Возникла непредвиденная ошибка!',
                position: 'bottom right',
                type: 'danger',
                duration: 3500,
            })
        })

    },

    GET_AUTH(state) {
        if (state.token) {
            state.isAuthenticated = true
        }
    },
}
