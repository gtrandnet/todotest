<?php

Route::post('login', 'Auth\AuthController@login')->middleware('cors');
Route::post('register', 'Auth\AuthController@register')->middleware('cors');

Route::group(['middleware' => ['auth.jwt', 'cors']], function () {
    Route::get('logout', 'Auth\AuthController@logout');

    Route::namespace('User')->group(function () {
        Route::group(['prefix' => 'user'], function () {
            Route::post('/', 'UserController@store');
            Route::get('/', 'UserController@getAll');
            Route::put('/{userId}', 'UserController@update');
            Route::delete('/{userId}', 'UserController@destroy');
        });
    });

    Route::namespace('Todo')->group(function () {
        Route::group(['prefix' => 'user/todo'], function () {
            Route::post('/', 'TodoController@store');
            Route::get('/{userId}', 'TodoController@getUserTodos');
            Route::put('/{todoId}', 'TodoController@update');
            Route::delete('/{todoId}', 'TodoController@destroy');
        });
    });

});