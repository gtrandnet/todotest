<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Modules\User\Repositories\UserRepository;
use App\Modules\User\Requests\CreateUserRequest;
use App\Modules\User\Requests\UpdateUserRequest;
use App\Modules\User\UseCases\UserUseCase;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var UserUseCase
     */
    private $userUseCase;

    /**
     * UserController constructor.
     * @param UserRepository $userRepository
     * @param UserUseCase $userUseCase
     */
    public function __construct(UserRepository $userRepository, UserUseCase $userUseCase)
    {
        $this->userRepository = $userRepository;
        $this->userUseCase = $userUseCase;
    }

    /**
     * @return array
     */
    public function getAll()
    {
        $users = $this->userRepository->getAll();

        return compact('users');
    }

    /**
     * @param CreateUserRequest $request
     * @return array
     */
    public function store(CreateUserRequest $request)
    {
        $user = $this->userUseCase->create($request);

        return compact('user');
    }

    /**
     * @param UpdateUserRequest $request
     * @param $userId
     * @return array
     */
    public function update(UpdateUserRequest $request, $userId)
    {
        $user = $this->userUseCase->update($request, $userId);

        return compact('user');
    }

    /**
     * @param $userId
     */
    public function destroy($userId):void
    {
        $this->userUseCase->destroy($userId);
    }
}
