<?php

namespace App\Http\Controllers\Todo;

use App\Http\Controllers\Controller;
use App\Modules\Todo\Repositories\TodoRepository;
use App\Modules\Todo\Requests\CreateTodoRequest;
use App\Modules\Todo\Requests\UpdateTodoRequest;
use App\Modules\Todo\UseCases\TodoUseCase;

class TodoController extends Controller
{
    /**
     * @var TodoRepository
     */
    private $todoRepository;

    /**
     * @var TodoUseCase
     */
    private $todoUseCase;

    /**
     * TodoController constructor.
     * @param TodoRepository $todoRepository
     * @param TodoUseCase $todoUseCase
     */
    public function __construct(TodoRepository $todoRepository, TodoUseCase $todoUseCase)
    {
        $this->todoRepository = $todoRepository;
        $this->todoUseCase = $todoUseCase;
    }

    /**
     * @param $userId
     * @return array
     */
    public function getUserTodos($userId)
    {
        $user = $this->todoRepository->getTodos($userId);

        return compact('user');
    }

    /**
     * @param CreateTodoRequest $request
     * @return array
     */
    public function store(CreateTodoRequest $request)
    {
        $todo = $this->todoUseCase->create($request);

        return compact('todo');
    }

    /**
     * @param UpdateTodoRequest $request
     * @param $todoId
     * @return array
     */
    public function update(UpdateTodoRequest $request, $todoId)
    {
        $todo = $this->todoUseCase->update($request, $todoId);

        return compact('todo');
    }

    /**
     * @param $todoId
     */
    public function destroy($todoId)
    {
        $this->todoUseCase->destroy($todoId);
    }
}
