<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/22/2020
 * Time: 6:08 AM
 */

namespace App\Modules\Todo\Repositories;


use App\Modules\Auth\Models\User;

class TodoRepository
{
    /**
     * @param $userId
     * @return mixed
     */
    public function getTodos($userId)
    {
        $user = User::find($userId);

        $user->todos;

        return $user;
    }
}