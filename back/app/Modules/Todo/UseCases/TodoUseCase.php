<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 5/22/2020
 * Time: 6:54 AM
 */

namespace App\Modules\Todo\UseCases;


use App\Modules\Auth\Models\User;
use App\Modules\Todo\Models\Todo;

class TodoUseCase
{
    /**
     * @param $request
     * @return Todo
     */
    public function create($request):Todo
    {
        $user = User::find($request->user_id);

        $todo = new Todo();
        $todo->title = $request->title;

        $user->todos()->save($todo);

        return $todo;
    }

    /**
     * @param $request
     * @param $todoId
     * @return Todo
     */
    public function update($request, $todoId):Todo
    {
        $todo = Todo::findOrFail($todoId);
        $todo->title = $request->title;
        $todo->update();

        return $todo;
    }

    /**
     * @param $todoId
     */
    public function destroy($todoId)
    {
        $todo = Todo::find($todoId);

        $todo->delete();
    }
}