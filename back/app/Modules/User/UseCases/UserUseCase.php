<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 4/25/2020
 * Time: 4:25 PM
 */

namespace App\Modules\User\UseCases;



use App\Modules\Auth\Models\User;

class UserUseCase
{
    /**
     * @param $request
     * @return User
     */
    public function create($request):User
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        return $user;
    }

    /**
     * @param $request
     * @param $userId
     * @return User
     */
    public function update($request, $userId):User
    {
        $user = User::find($userId);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->update();

        return $user;
    }

    /**
     * @param $userId
     */
    public function destroy($userId):void
    {
        $user = User::findOrFail($userId);

        $user->delete();
    }
}