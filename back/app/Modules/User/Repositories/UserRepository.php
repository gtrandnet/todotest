<?php


namespace App\Modules\User\Repositories;


use App\Modules\Auth\Models\User;

class UserRepository
{
    /**
     * @return mixed
     */
    public function getAll()
    {
        $limit = 7;
        $users = User::paginate($limit);

        return $users;
    }
}