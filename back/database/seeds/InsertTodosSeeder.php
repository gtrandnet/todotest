<?php

use Illuminate\Database\Seeder;

class InsertTodosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\Modules\Auth\Models\User::all();
        $this->command->getOutput()->progressStart(count($users));
         foreach ($users as $user) {
             $this->createTodo($user->id);

             $this->command->getOutput()->progressAdvance();
         }

        $this->command->getOutput()->progressFinish();
    }

    /**
     * @param $userId
     */
    private function createTodo($userId):void
    {
        $faker = Faker\Factory::create();
        $data = [
            ['title' => $faker->name, 'user_id' => $userId],
            ['title' => $faker->name, 'user_id' => $userId],
            ['title' => $faker->name, 'user_id' => $userId]
        ];
        \Illuminate\Support\Facades\DB::table('todos')->insert($data);
    }
}
